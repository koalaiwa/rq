from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template.context import RequestContext

# Create your views here.
def index(request):
    return render(request, 'personal/home.html')

def contact(request):
    return render(request, 'personal/basic.html', {'content': ['If you would like to contact me, please email me', 'gogomrak@gmail.com']})

def home(request):
    context = RequestContext(request, {'request': request, 'user': request.user})
    return render_to_response('personal/home.html', context_instance=context)