from django.conf.urls import url, include
from . import views
from django.contrib import admin
admin.autodiscover()

# Create your views here.
urlpatterns=[
    url(r'^$', views.index, name='index'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include('django.contrib.auth.urls', namespace='auth')),
]